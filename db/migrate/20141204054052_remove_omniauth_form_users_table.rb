class RemoveOmniauthFormUsersTable < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up   {
        remove_index :users, :username
        remove_column :users, :username, :string, null: false, default: ''
      }
      dir.down {
        add_column :users, :username, :string, null: false, default: ''
        add_index :users, :username
      }
    end

    remove_column :users, :provider, :string
    remove_column :users, :uid, :string
    remove_column :users, :image, :string
    remove_column :users, :url, :string

    reversible do |dir|
      dir.up   {
        User.delete_all
        Post.delete_all
        Comment.delete_all
      }
      dir.down {
      }
    end
    add_index :users, :email, unique: true
  end
end

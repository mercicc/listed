class CreateGlobalizeCategories < ActiveRecord::Migration
  def up
    create_table :categories do |t|
      t.timestamps
    end
    Category.create_translation_table! :name => :string

    categories_default_name = Array.new
    categories_default_name << { en: 'Business',      :'zh-TW' => '商業' }
    categories_default_name << { en: 'Communication', :'zh-TW' => '通訊' }
    categories_default_name << { en: 'Education',     :'zh-TW' => '教育' }
    categories_default_name << { en: 'Entertainment', :'zh-TW' => '娛樂' }
    categories_default_name << { en: 'Fashion',       :'zh-TW' => '流行' }
    categories_default_name << { en: 'Finance',       :'zh-TW' => '金融' }
    categories_default_name << { en: 'Food',          :'zh-TW' => '飲食' }
    categories_default_name << { en: 'Game',          :'zh-TW' => '遊戲' }
    categories_default_name << { en: 'Health',        :'zh-TW' => '健康' }
    categories_default_name << { en: 'Local',         :'zh-TW' => '在地' }
    categories_default_name << { en: 'Music',         :'zh-TW' => '音樂' }
    categories_default_name << { en: 'Navigation',    :'zh-TW' => '導航' }
    categories_default_name << { en: 'News',          :'zh-TW' => '新聞' }
    categories_default_name << { en: 'Photo',         :'zh-TW' => '攝影' }
    categories_default_name << { en: 'Productivity',  :'zh-TW' => '生產力' }
    categories_default_name << { en: 'Reference',     :'zh-TW' => '參考' }
    categories_default_name << { en: 'Shopping',      :'zh-TW' => '購物' }
    categories_default_name << { en: 'Sport',         :'zh-TW' => '運動' }
    categories_default_name << { en: 'Travel',        :'zh-TW' => '旅行' }
    categories_default_name << { en: 'Utilities',     :'zh-TW' => '工具' }
    categories_default_name << { en: 'Video',         :'zh-TW' => '影片' }
    categories_default_name << { en: 'Weather',       :'zh-TW' => '氣象' }
    categories_default_name << { en: 'Other',         :'zh-TW' => '其他' }

    categories_default_name.each do |name|
      I18n.locale = 'en'
      category = Category.new({name: name[:en]})
      category.save
      I18n.locale = 'zh-TW'
      category.reload
      category.name = name[:'zh-TW']
      category.save
    end
  end

  def down
    drop_table :categories
    Category.drop_translation_table!
  end
end

module ApplicationHelper
  def avatar_image(image_path)
    image_path = !image_path.nil? ? image_path : 'empty_avatar.png'
    image_tag image_path
  end

  def timeago(time, options = {})
    options[:class] ||= 'timeago'
    content_tag(:abbr, time.to_s, options.merge(title: time.getutc.iso8601)) if time
  end
end

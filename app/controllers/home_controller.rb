class HomeController < ApplicationController
  def index
    if params[:category]
      @posts = Post.category_with(params[:category])
    elsif params[:tag]
      @posts = Post.tagged_with(params[:tag])
    else
      @posts = Post.all
    end
  end
end

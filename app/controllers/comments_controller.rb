class CommentsController < ApplicationController
  before_action :set_comment, only: [:edit, :update, :destroy]
  before_action :authenticate_user!

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    respond_to do |format|
      if @comment.save
        format.js { render 'create', locals: { counts: Comment.all.size } }
      else
        format.js { render 'create_error' }
      end
    end
  end

  def edit
    respond_to { |format| format.js }
  end

  def update
    respond_to do |format|
      @comment.update(comment_params) ? format.js : format.js { render 'edit' }
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.js do
        render 'destroy', locals: { counts: Comment.all.size, id: params[:id] }
      end
    end
  end

  private

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:content, :post_id, :user_id)
    end
end

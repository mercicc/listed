$ ->
  $('[data-toggle="tooltip"]').tooltip()

  # remove the url word '#_=_' after OmniAuth Facebook login. 
  if window.location.hash and window.location.hash == '#_=_'
    window.location.hash = ''
    history.pushState('', document.title, window.location.pathname)

  # li.post clickable
  $('li.post').on 'click', (e)->
    if $(e.target).closest('.vote-box').length  == 0 and
       $(e.target).closest('.title').length     == 0 and
       $(e.target).closest('.avatar').length    == 0 and
       $(e.target).closest('.comments').length  == 0 and
       $(e.target).closest('.acts').length      == 0 and
       $(e.target).closest('.tag_list').length  == 0 and
       $(e.target).closest('.category').length  == 0
      location.href = $(@).data('url')

  # .upvote ajax
  $('.votable').on 'click', '.upvote', ->
    $.ajax({
      url: $(@).data('url')
      dataType: 'script'
    })

  # timeago
  $('abbr.timeago').timeago();

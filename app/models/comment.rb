class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user

  validates :content, presence: true

  acts_as_votable

  delegate :name, :image, to: :user, prefix: true

  def editable_by?(user)
    user && user == self.user
  end
end

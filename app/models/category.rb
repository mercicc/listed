class Category < ActiveRecord::Base
  has_many :posts

  translates :name
end

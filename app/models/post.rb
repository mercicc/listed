class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :comments, dependent: :destroy

  validates :title, presence: true
  validates :url, presence: true
  validates :intro, presence: true
  validates :category_id, presence: true

  acts_as_votable
  acts_as_taggable

  delegate :name, :image, to: :user, prefix: true

  def to_param
    "#{id} #{title}".to_slug.normalize.to_s
  end

  def self.category_with(name)
    category = Category.with_translations.where(name: name).first
    posts = !category.nil? ? category.posts : {}
    posts
  end

  def editable_by?(user)
    user && user == self.user
  end
end

class User < ActiveRecord::Base
  include OmniauthCallbacks

  has_many :posts
  has_many :comments

  acts_as_voter

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable,
         :omniauthable, omniauth_providers: [:facebook, :github]

  def name
    self.email.split(/@/).first
  end

  def to_param
    "#{id} #{name}".to_slug.normalize.to_s
  end

  def image
    omniauths.first.image
  end
end

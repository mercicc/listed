# listed

A list for my collection of startup products, news, and etc.

## Prerequisites
* Ruby 2.1.4+
* Rails 4.1.7+
* SQLite 3.8.5+

## Setup
Install dependencies
```shell
$ bundle install
```
Default setup
```shell
$ rake db:setup
```
Migration setup
```shell
$ rake db:migrate
$ rake db:seed

## Basic run
Start rails server, WEBrick:
```shell
$ rails s
```
or Thin:
```shell
$ thin start
```

Visit [http://localhost:3000](http://localhost:3000)

## Run with Pow in development stage
Install Pow
```shell
curl get.pow.cx | sh
```
Install Powder(manage pow)
```shell
gem install powder
```
Usage
```
cd /path/to/rails/app
powder link
powder open
```
Restart Pow Application
```
touch tmp/restart.txt
```
Restart Pow Server(When pow or app environment configuration changes)
```
touch ~/.pow/restart.txt
```

## OmniAuth KEY & SECRET setting
```
cp config/application.example.yml config/application.yml
```
use figaro set heroku config
```
figaro heroku:set -e production
```

## Use rails_best_practices to check the quality of rails codes.
Install
```
gem install rails_best_practices
```
Usage
```
# At the root directory of rails app
rails_best_practices .
```

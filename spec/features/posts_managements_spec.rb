require 'rails_helper'

feature "PostsManagements", :type => :feature do
  context 'when user login' do
    scenario 'can share a new production' do
      user = FactoryGirl.create(:user)
      page.set_rack_session(:user_id => user.id)
      visit new_post_path
      within("#new_post") do
        fill_in 'post[title]', :with => 'title'
        fill_in 'post[url]', :with => 'http://localhost'
        fill_in 'post[intro]', :with => 'intro'
      end
      click_button '新增'
      expect(page).to have_content 'Listed'
    end
  end
end

FactoryGirl.define do
  factory :user do
    provider 'github'
    uid '11111'
    name 'admin'
  end
end

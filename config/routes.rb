Rails.application.routes.draw do
  root 'home#index'

  ## User
  devise_for :user, path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'signup' },
                    controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  resources :users, only: [:edit, :update]
  get ':id/profile' => 'users#show',  as: :user_profile
  get ':id/posts'    => 'posts#index', as: :user_made
  get ':id/setting' => 'users#edit', as: :user_setting
  match ':id/setting' => 'users#update', via: [:put, :patch]
  get ':id/change_password' => 'users#edit_password', as: :user_change_password
  match ':id/change_password' => 'users#update_password', via: [:put, :patch]

  ## Posts
  resources :posts, except: [:index] do
    resources :comments, only: [:create]
  end
  get 'posts' => 'home#index'

  get 'categories/:category', to: 'home#index', as: :category

  get 'tags/:tag', to: 'home#index', as: :tag

  ## Comments
  resources :comments, only: [:edit, :update, :destroy]

  ## Votes
  get ':model/:id/vote/up' => 'votes#upvote', as: :upvote
  get ':model/:id/vote/down' => 'votes#downvote', as: :downvote
end
